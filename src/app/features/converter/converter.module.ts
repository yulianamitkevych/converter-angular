import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConverterComponent} from './converter.component';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ConverterComponent
  ],
  exports: [
    ConverterComponent
  ],
  imports: [
    CommonModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule
  ]
})
export class ConverterModule {
}
