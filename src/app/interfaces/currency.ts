export interface CurrencyInfo {
  ccy: string
  base_ccy: string;
  buy: number;
  sale: number;
}

export interface CurrencyInfoRes {
  ccy: string
  base_ccy: string;
  buy: string;
  sale: string;
}
