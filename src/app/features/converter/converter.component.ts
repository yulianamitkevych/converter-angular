import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from "@angular/forms";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {CurrencyInfo} from "../../interfaces/currency";
import {ConverterService} from "../../services/converter.service";

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.scss'],
})

export class ConverterComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  currencyForm: FormGroup;
  UAH = {'ccy': 'UAH', base_ccy: "UAH", "buy": 1, "sale": 1};
  currencyList: CurrencyInfo[] = [];

  constructor(
    private fb: FormBuilder,
    private converterService: ConverterService
  ) {
  }

  ngOnInit(): void {
    this.getCurrencyList();
    this.initializeForm();
    console.log(this.currencyForm)
  }

  onChange(input: string): void {
    this.convert(input);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  private getCurrencyList(): void {
    this.converterService.getCurrencyInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(res => {
        this.currencyList = res.slice(0, -1);
      })
  }

  private initializeForm(): void {
    this.currencyForm = this.fb.group({
      convertible: '',
      converting: '',
      convertibleSum: '',
      convertedSum: ''
    })
  }

  private convert(input: string): void {
    const {convertible, converting, convertibleSum, convertedSum} = this.currencyForm.value;

    if (input === 'Convertible') {
      if (this.currencyForm.value.convertible.ccy !== 'UAH') {
        const convertingRates = convertible / converting;

        this.currencyForm.patchValue({
          convertedSum: (convertibleSum * convertingRates).toFixed(2)
        });
      } else {
        this.currencyForm.patchValue({
          convertedSum: (convertibleSum * converting).toFixed(2)
        });
      }
    } else {
      if (this.currencyForm.value.converting.ccy !== 'UAH') {
        const convertingRates = converting / convertible;

        this.currencyForm.patchValue({
          convertibleSum: (convertedSum * convertingRates).toFixed(2)
        });
      } else {
        this.currencyForm.patchValue({
          convertibleSum: (convertedSum * converting).toFixed(2)
        });
      }
    }
  }
}
