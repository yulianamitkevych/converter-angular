import {Component, OnInit, OnDestroy} from '@angular/core';

import {takeUntil} from 'rxjs/operators';
import {Subject} from "rxjs";
import {CurrencyInfo} from '../../interfaces/currency';
import {ConverterService} from '../../services/converter.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  currencyList: CurrencyInfo[] = [];
  displayedColumns: string[] = ['ccy', 'buy', 'sale'];

  constructor(private converterService: ConverterService) {
  }

  ngOnInit(): void {
    this.getCurrencyList()
  }

  private getCurrencyList(): void {
    this.converterService.getCurrencyInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(res => {
        this.currencyList = res.slice(0,-1);
      })
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
