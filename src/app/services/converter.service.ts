import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {CurrencyInfo, CurrencyInfoRes} from '../interfaces/currency';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor(private http: HttpClient) {
  }

  getCurrencyInfo(): Observable<CurrencyInfo[]> {
    return this.http.get<CurrencyInfoRes[]>('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11')
      .pipe(
        map(res => res.map(currency => ({
          ...currency, buy: Number(currency.buy), sale: Number(currency.sale)
        })))
      )
  }
}
